To get all issue and volume without endless loop use the script:

`getAllIssuesID.js`
To use it run in the javascript consol of you browser, launched from pages:


`https://www.ams.org/journals/tran/all_issues.html`
and
`https://www.ams.org/journals/bull/all_issues.html`


Then copy paste the result in a text file and clean it (ie remove everythign which is not an html link).

The `getAllIssuesID.js` script is badly handling the leading zero. It should be fixed but if not you can this vim expression:  (this should have been fix, you need to use the function `pad` from `getAllIssuesID.js`)
```vi
%s/0\([0-9]\)\([0-9]\)/\1\2/g
```
you can then put together the two file in `amsAllIssues.csv`

```
cat allBULLhtml.csv allTAMShtml.csv > amsAllIssues.csv
```

and then run `getAll.sh`

```bash
bash getAll.sh amsAllIssues.csv 
```

this will create a folder html and a folder data. In html all webpage of all issue will be stored and used to extract all the pdf available in the issue's webpage. 

Then using the perl script it become possible to extract intersting values from the html to make  a central csv

```bash 
perl bullParser.pl #doesn't handle filename, should be corrected somehow, 
```
