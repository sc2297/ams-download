#!/bin/bash
#Script qui recuperer tous les pdf des journaux de ams a partir dune liste avec les different issues et volumes disponible sur le site de l'AMS
fil=$1

for i in `cat $fil` 
do
    echo $i
    dir=`dirname $i`  #this get the issue with the full html adress in ams webpage 
    jour=`dirname $dir` #this get the journal with the full html adress in ams webpage 
    journal=`basename $jour` #this get the journal id alone
    issue=`basename $dir` #this get the issue id alone

	if [ ! -d "data/$journal/$issue" ]; then
		mkdir -p "data/$journal/$issue"
	fi
	if [ ! -d "html/$journal/$issue" ]; then
		mkdir -p "html/$journal/"
	fi

    echo "downloading issue $issue from $journal"
    wget $i -O html/${journal}/${issue} -o log  #download the html page of the issue (with all link to pdf and more)
    cat html/${journal}/${issue} | grep -o "href=\".*\.pdf\"" | sed "s/\"//g" | sed "s/href=//">allpdf #extract from the page the pdf adresses

    for pdf in `cat allpdf` #download the pdfs
    do
        paperid=`basename $pdf`
        urlpaper="https://www.ams.org/journals/"${journal}"/"${issue}"/"${pdf}
        echo $urlpaper
        echo "downloading paper $paperid from issue $issue from $journal"
		wget $urlpaper -O data/$journal/$issue/${paperid} -o log$jtype
        pdftotext data/$journal/$issue/${paperid}
        rm data/$journal/$issue/${paperid} #Commenter ici pour stocker les pdf
    done 

done
